import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';
import { Profile } from '@kominal/ecosystem-models/profile';
import { RefreshResponse } from '@kominal/ecosystem-models/refresh-response';
import { Session } from '@kominal/ecosystem-models/session';
import { EcosystemClientModule } from '../../ecosystem-client.module';

@Injectable({
	providedIn: 'root',
})
export class UserHttpService {
	constructor(private ecosystemClientModule: EcosystemClientModule, private httpClient: HttpClient) {}

	getServiceURL() {
		return `https://${this.ecosystemClientModule.ecosystemBaseUrl}/user-service`;
	}

	getSessions(): Promise<Session[]> {
		return this.httpClient.get<Session[]>(`${this.getServiceURL()}/list`).toPromise();
	}

	refreshToken(sessionKey: string): Promise<RefreshResponse> {
		return this.httpClient
			.post<RefreshResponse>(
				`${this.getServiceURL()}/refresh`,
				{},
				{
					headers: {
						Authorization: `Bearer ${sessionKey}`,
					},
				}
			)
			.toPromise();
	}

	removeSession(sessionId: string): Promise<void> {
		return this.httpClient.delete<void>(`${this.getServiceURL()}/remove/${sessionId}`).toPromise();
	}

	login(username: string, password: string): Promise<AESEncrypted> {
		return this.httpClient
			.post<AESEncrypted>(`${this.getServiceURL()}/login`, { username, password })
			.toPromise();
	}

	logout(sessionKey: string): Promise<void> {
		return this.httpClient
			.post<void>(
				`${this.getServiceURL()}/logout`,
				{},
				{
					headers: {
						Authorization: `Bearer ${sessionKey}`,
					},
				}
			)
			.toPromise();
	}

	register(payload: {
		username: string;
		displaynameHash: string;
		password: string;
		resetToken: string;
		resetData: AESEncrypted;
		masterEncryptionKey: AESEncrypted;
		privateKey: AESEncrypted;
		publicKey: AESEncrypted;
		displayname: AESEncrypted;
		profilePublicKey: AESEncrypted;
		keyDisplayname: AESEncrypted;
		keyMasterEncryptionKey: AESEncrypted;
	}): Promise<void> {
		return this.httpClient.post<void>(`${this.getServiceURL()}/register`, payload).toPromise();
	}

	changePassword(payload: {
		password: string;
		newPassword: string;
		resetToken: string;
		resetData: AESEncrypted;
		masterEncryptionKey: AESEncrypted;
	}): Promise<void> {
		return this.httpClient.post<void>(`${this.getServiceURL()}/changePassword`, payload).toPromise();
	}

	createSession(key: string, username: string, password: string, masterEncryptionKey: AESEncrypted, device: AESEncrypted): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceURL()}/sessions`, { key, username, password, masterEncryptionKey, device })
			.toPromise();
	}

	getPublicKey(displayname: string): Promise<AESEncrypted> {
		return this.httpClient
			.post<AESEncrypted>(`${this.getServiceURL()}/users/publicKey`, { displayname })
			.toPromise();
	}

	getId(displayname: string): Promise<string> {
		return this.httpClient
			.post<string>(`${this.getServiceURL()}/users/id`, { displayname })
			.toPromise();
	}

	createProfile(
		displayname: AESEncrypted,
		publicKey: AESEncrypted,
		displaynameHash: string,
		keyDisplayname: AESEncrypted,
		keyMasterEncryptionKey: AESEncrypted
	): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceURL()}/profile`, { displayname, publicKey, displaynameHash, keyDisplayname, keyMasterEncryptionKey })
			.toPromise();
	}

	get(profielId: string, updatedAt?: number): Promise<Profile> {
		return this.httpClient.get<Profile>(`${this.getServiceURL()}/profile/${profielId}${updatedAt ? '/' + updatedAt : ''}`).toPromise();
	}

	getByDisplaynameHash(displaynameHash: string): Promise<Profile> {
		return this.httpClient.get<Profile>(`${this.getServiceURL()}/profile/displayname/${encodeURIComponent(displaynameHash)}`).toPromise();
	}

	listProfiles(): Promise<Profile[]> {
		return this.httpClient.get<Profile[]>(`${this.getServiceURL()}/profile`).toPromise();
	}

	updateProfile(
		profileId: string,
		state: {
			primary?: boolean;
			avatar?: AESEncrypted;
			displaynameHash?: string;
			displayname?: AESEncrypted;
			keyDisplayname?: AESEncrypted;
		}
	): Promise<void> {
		return this.httpClient.put<void>(`${this.getServiceURL()}/profile/${profileId}`, state).toPromise();
	}

	deleteProfile(profileId: string): Promise<void> {
		return this.httpClient.delete<void>(`${this.getServiceURL()}/profile/${profileId}`).toPromise();
	}

	deleteAccount(password: string): Promise<void> {
		return this.httpClient
			.post<void>(`${this.getServiceURL()}/deleteAccount`, { password })
			.toPromise();
	}
}
