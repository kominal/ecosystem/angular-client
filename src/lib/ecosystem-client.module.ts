import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserHttpService } from './http/user/user-http.service';

@NgModule({
	declarations: [],
	imports: [CommonModule],
	providers: [UserHttpService],
})
export class EcosystemClientModule {
	constructor(public ecosystemBaseUrl?: string) {
		ecosystemBaseUrl = ecosystemBaseUrl || 'ecosystem-test.kominal.com';
	}
}
